import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:interview_task1/src/data/model/album.dart';
import 'package:interview_task1/src/data/model/photos.dart';
import 'package:interview_task1/src/data/utils/service_contants.dart';

abstract class AlbumDataProvider {
  Future<Albums> fetchAlbum();

  Future<bool> deleteAlbum(int id);

  Future<Album> addToAlbum(Album album);

  Future<List<Photos>> getAlbumPhotos(int id);
}

class AlbumDataProviderImp implements AlbumDataProvider {
  @override
  Future<Albums> fetchAlbum() async {
    final response = await http.get(Uri.parse(ServiceEndpoints.album.path));

    if (response.statusCode == 200) {
      final map = jsonDecode(response.body);
      return Albums.fromJson(map);
    } else {
      throw Exception('Failed');
    }
  }

  @override
  Future<bool> deleteAlbum(int id) async {
    final http.Response response = await http.delete(
      Uri.parse('${ServiceEndpoints.album.path}/$id'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Failed.');
    }
  }

  @override
  Future<Album> addToAlbum(Album album) async {
    final http.Response response = await http.post(
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        Uri.parse(ServiceEndpoints.album.path),
        body: jsonEncode(album.toJson()));
    if (response.statusCode == 201) {
      final map = jsonDecode(response.body);

      return Album.fromJson(map);
    } else {
      throw Exception('Failed.');
    }
  }

  @override
  Future<List<Photos>> getAlbumPhotos(int id) async {
    final response =
        await http.get(Uri.parse('${ServiceEndpoints.album.path}/$id/photos'));

    if (response.statusCode == 200) {
      final List<dynamic> map = jsonDecode(response.body);
      return map.map((e) => Photos.fromJson(e)).toList();
    } else {
      throw Exception('Failed');
    }
  }
}

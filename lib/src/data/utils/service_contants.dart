enum ServiceEndpoints {
  album(path: 'https://jsonplaceholder.typicode.com/albums');

  final String path;

  const ServiceEndpoints({required this.path});
}

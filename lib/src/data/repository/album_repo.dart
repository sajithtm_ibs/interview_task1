import 'package:interview_task1/src/data/model/album.dart';
import 'package:interview_task1/src/data/model/photos.dart';
import 'package:interview_task1/src/data/provider/album_data_provider.dart';

abstract class AlbumRepo {
  Albums? albums;
  Future loadAlbum();
  Future<bool> deleteAlbum(int id);
  Future<bool> addToAlbum();
  Future<List<Photos>> getPhotos(int id);
}

class AlbumRepoImp extends AlbumRepo {
  final AlbumDataProvider provider;

  AlbumRepoImp({required this.provider});
  @override
  Future loadAlbum() async {
    albums = await provider.fetchAlbum();
  }

  @override
  Future<bool> deleteAlbum(int id) {
    return provider.deleteAlbum(id);
  }

  @override
  Future<bool> addToAlbum() async {
    const album = Album(userId: 1, id: 1, title: 'lorem ipsum');
    final al = await provider.addToAlbum(album);
    albums?.albums?.insert(0, al);
    return true;
  }

  @override
  Future<List<Photos>> getPhotos(int id) {
    return provider.getAlbumPhotos(id);
  }
}

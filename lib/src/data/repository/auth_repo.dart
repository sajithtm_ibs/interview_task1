import 'dart:convert';

import 'package:interview_task1/src/di/locator.dart';
import 'package:interview_task1/src/utils/shared_pref.dart';

abstract class AuthRepo {
  Future saveCredentials({
    required String username,
    required String password,
  });

  Future<bool> validateCredentials({
    required String username,
    required String password,
  });

  bool isLoggedIn();

  bool isOnBoarded();

  setLoggedInFlag();
}

class AuthRepoImp extends AuthRepo {
  @override
  bool isLoggedIn() {
    return getIt
            .get<SharedPref>()
            .preferences
            .getString(SharedPrefKeys.isLoggedIn.key) !=
        null;
  }

  @override
  Future saveCredentials(
      {required String username, required String password}) async {
    //TODO: convert to a model
    String jsonData =
        jsonEncode({'username': username, 'password': password}).toString();
    await getIt
        .get<SharedPref>()
        .preferences
        .setString(SharedPrefKeys.credentials.key, jsonData);
  }

  @override
  Future<bool> validateCredentials(
      {required String username, required String password}) async {
    final cred = getIt
        .get<SharedPref>()
        .preferences
        .getString(SharedPrefKeys.credentials.key);
    if (cred == null) return false;
    final Map<String, dynamic> map = jsonDecode(cred);
    return (map['username'] == username && map['password'] == password);
  }

  @override
  setLoggedInFlag() async {
    await getIt
        .get<SharedPref>()
        .preferences
        .setBool(SharedPrefKeys.isLoggedIn.key, true);
  }

  @override
  bool isOnBoarded() {
    return getIt
            .get<SharedPref>()
            .preferences
            .getString(SharedPrefKeys.credentials.key) !=
        null;
  }
}

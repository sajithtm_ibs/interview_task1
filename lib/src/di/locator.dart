import 'package:get_it/get_it.dart';
import 'package:interview_task1/src/bloc/auth/auth_bloc.dart';
import 'package:interview_task1/src/bloc/album/album_bloc.dart';
import 'package:interview_task1/src/data/provider/album_data_provider.dart';
import 'package:interview_task1/src/data/repository/album_repo.dart';
import 'package:interview_task1/src/data/repository/auth_repo.dart';

import '../utils/shared_pref.dart';

final getIt = GetIt.instance;

setupLocator() {
  getIt.registerSingleton<SharedPref>(SharedPref());
  getIt.registerFactory<AuthRepo>(
    () => AuthRepoImp(),
  );
  getIt.registerFactory<AlbumRepo>(
    () => AlbumRepoImp(provider: AlbumDataProviderImp()),
  );
  getIt.registerFactory<AuthBloc>(() => AuthBloc(getIt.get<AuthRepo>()));
  getIt.registerFactory<AlbumBloc>(() => AlbumBloc(getIt.get<AlbumRepo>()));
}

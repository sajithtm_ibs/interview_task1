// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;
import 'package:interview_task1/src/data/model/photos.dart' as _i6;
import 'package:interview_task1/src/presentation/screens/album_photo/album_photos.dart'
    as _i1;
import 'package:interview_task1/src/presentation/screens/auth/auth_screen.dart'
    as _i2;
import 'package:interview_task1/src/presentation/screens/home/home_screen.dart'
    as _i3;

abstract class $AppRouter extends _i4.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    AlbumRoute.name: (routeData) {
      final args = routeData.argsAs<AlbumRouteArgs>();
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i1.AlbumPhotosGrid(
          key: args.key,
          photos: args.photos,
        ),
      );
    },
    AuthRoute.name: (routeData) {
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i4.WrappedRoute(child: const _i2.AuthScreen()),
      );
    },
    HomeRoute.name: (routeData) {
      return _i4.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i4.WrappedRoute(child: const _i3.HomeScreen()),
      );
    },
  };
}

/// generated route for
/// [_i1.AlbumPhotosGrid]
class AlbumRoute extends _i4.PageRouteInfo<AlbumRouteArgs> {
  AlbumRoute({
    _i5.Key? key,
    required List<_i6.Photos> photos,
    List<_i4.PageRouteInfo>? children,
  }) : super(
          AlbumRoute.name,
          args: AlbumRouteArgs(
            key: key,
            photos: photos,
          ),
          initialChildren: children,
        );

  static const String name = 'AlbumRoute';

  static const _i4.PageInfo<AlbumRouteArgs> page =
      _i4.PageInfo<AlbumRouteArgs>(name);
}

class AlbumRouteArgs {
  const AlbumRouteArgs({
    this.key,
    required this.photos,
  });

  final _i5.Key? key;

  final List<_i6.Photos> photos;

  @override
  String toString() {
    return 'AlbumRouteArgs{key: $key, photos: $photos}';
  }
}

/// generated route for
/// [_i2.AuthScreen]
class AuthRoute extends _i4.PageRouteInfo<void> {
  const AuthRoute({List<_i4.PageRouteInfo>? children})
      : super(
          AuthRoute.name,
          initialChildren: children,
        );

  static const String name = 'AuthRoute';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

/// generated route for
/// [_i3.HomeScreen]
class HomeRoute extends _i4.PageRouteInfo<void> {
  const HomeRoute({List<_i4.PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i4.PageInfo<void> page = _i4.PageInfo<void>(name);
}

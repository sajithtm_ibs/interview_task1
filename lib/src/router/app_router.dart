import 'package:auto_route/auto_route.dart';
import 'package:interview_task1/src/di/locator.dart';
import 'package:interview_task1/src/router/app_router.gr.dart';
import 'package:interview_task1/src/utils/shared_pref.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.material();

  final isLoggedIn = getIt
          .get<SharedPref>()
          .preferences
          .getBool(SharedPrefKeys.isLoggedIn.key) ??
      false;
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: AuthRoute.page, initial: !isLoggedIn),
        AutoRoute(page: HomeRoute.page, initial: isLoggedIn),
        AutoRoute(page: AlbumRoute.page)
        // AutoRoute(page: IntroRouteController.page, initial: !isIntroSeen),
        // AutoRoute(
        //     page: DashboardRoute.page,
        //     // guards: [IntroGuard()],
        //     path: '/',
        //     children: [
        //       AutoRoute(
        //           page: InitialRouter.page,
        //           maintainState: true,
        //           children: [
        //             AutoRoute(page: DriveList.page, path: ''),
        //             AutoRoute(page: DriveSelection.page),
        //             AutoRoute(page: DriveDetails.page),
        //             AutoRoute(page: MediaCategorySelection.page),
        //             AutoRoute(page: TransferSourceMaster.page),
        //             AutoRoute(page: TransferSourceDetails.page),
        //             AutoRoute(page: BackupCircularProgress.page),
        //             AutoRoute(page: TransferDestination.page),
        //             AutoRoute(page: TransferSource.page),
        //             AutoRoute(page: SelectedDestination.page),
        //             AutoRoute(page: Encryption.page),
        //             AutoRoute(page: Security.page),
        //             AutoRoute(page: FreeupSpace.page),
        //             AutoRoute(page: UnlockDevice.page),
        //           ],
        //           keepHistory: true),
        //       AutoRoute(
        //           page: StoreInitialRouter.page,
        //           maintainState: true,
        //           children: [
        //             AutoRoute(page: StoreRoute.page, initial: true),
        //           ],
        //           keepHistory: true),
        //       AutoRoute(
        //           page: ExploreInitialRouter.page,
        //           maintainState: true,
        //           children: [
        //             AutoRoute(page: ExploreRoute.page, initial: true),
        //           ],
        //           keepHistory: true),
        //     ]),
      ];
}

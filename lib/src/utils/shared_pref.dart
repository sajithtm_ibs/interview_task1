import 'package:shared_preferences/shared_preferences.dart';

enum SharedPrefKeys {
  isLoggedIn(key: 'isLoIn'),
  credentials(key: 'cred');

  final String key;
  const SharedPrefKeys({required this.key});
}

class SharedPref {
  late final SharedPreferences preferences;
  Future loadPref() async {
    preferences = await SharedPreferences.getInstance();
  }
}

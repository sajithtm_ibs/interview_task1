import 'package:flutter/material.dart';

class AuthLayoutScreen extends StatefulWidget {
  const AuthLayoutScreen({
    super.key,
    required this.formKey,
    required this.onButtonTap,
    required this.buttonText,
    required this.usernameController,
    required this.passwordController,
  });

  final GlobalKey<FormState> formKey;
  final TextEditingController usernameController;
  final TextEditingController passwordController;
  final String buttonText;
  final Function()? onButtonTap;

  @override
  State<AuthLayoutScreen> createState() => _AuthLayoutScreenState();
}

class _AuthLayoutScreenState extends State<AuthLayoutScreen> {
  @override
  Widget build(BuildContext context) {
    return Form(
        key: widget.formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              decoration: const InputDecoration(hintText: 'Enter username'),
              controller: widget.usernameController,
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.length < 4) {
                  return 'Please provide username having length > 4.';
                }
                return null;
              },
            ),
            const SizedBox(
              height: 50,
            ),
            TextFormField(
              controller: widget.passwordController,
              obscureText: true,
              decoration: const InputDecoration(hintText: 'Enter password'),
              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.length < 4) {
                  return 'Please provide password having length > 4.';
                }
                return null;
              },
            ),
            const SizedBox(
              height: 100,
            ),
            ElevatedButton(
                style: ButtonStyle(
                    minimumSize:
                        MaterialStateProperty.all(const Size(150, 50))),
                onPressed: widget.onButtonTap,
                child: Text(
                  widget.buttonText,
                  style: Theme.of(context).textTheme.titleMedium,
                ))
          ],
        ));
  }
}

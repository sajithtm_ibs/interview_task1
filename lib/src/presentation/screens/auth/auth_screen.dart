import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:interview_task1/src/bloc/auth/auth_bloc.dart';
import 'package:interview_task1/src/di/locator.dart';
import 'package:interview_task1/src/presentation/common_widgets/auth_layout.dart';
import 'package:interview_task1/src/router/app_router.gr.dart';

@RoutePage()
class AuthScreen extends StatefulWidget implements AutoRouteWrapper {
  const AuthScreen({super.key});
  @override
  State<AuthScreen> createState() => _AuthScreenState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt.get<AuthBloc>(),
      child: this,
    );
  }
}

class _AuthScreenState extends State<AuthScreen> {
  final _formKey = GlobalKey<FormState>();
  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    context.read<AuthBloc>().add(CheckOnBoardStatusEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(seconds: 1),
      curve: Curves.fastOutSlowIn,
      child: BlocConsumer<AuthBloc, AuthState>(
        buildWhen: (previous, current) =>
            current is! OnLoginSuccessState && current is! OnFailureState,
        listener: (context, state) {
          if (state is OnFailureState) {
            showToast(state.error);
          } else if (state is OnLoginSuccessState) {
            context.router.replace(const HomeRoute());
          }
        },
        builder: (context, state) {
          switch (state.runtimeType) {
            case OnBoardSuccessState:
              userNameController.clear();
              passwordController.clear();
              return _scaffold('Login', true);
            default:
              return _scaffold('OnBoard', false);
          }
        },
      ),
    );
  }

  Scaffold _scaffold(String title, bool isLogin) {
    return Scaffold(
      appBar: AppBar(title: Text(title.toUpperCase())),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: AuthLayoutScreen(
              formKey: _formKey,
              passwordController: passwordController,
              usernameController: userNameController,
              onButtonTap: () {
                if (_formKey.currentState?.validate() ?? false) {
                  debugPrint('onButtonTap = success');
                  context.read<AuthBloc>().add(SaveOrValidateCredentialsEvent(
                        username: userNameController.text,
                        password: passwordController.text,
                        isOnboard: !isLogin,
                      ));
                  return;
                }
                showToast('Please provide credentials.');
              },
              buttonText: isLogin ? 'Login' : 'Save'),
        ),
      ),
    );
  }

  void showToast(String text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:interview_task1/src/data/model/photos.dart';

@RoutePage(name: 'AlbumRoute')
class AlbumPhotosGrid extends StatefulWidget {
  const AlbumPhotosGrid({super.key, required this.photos});
  final List<Photos> photos;

  @override
  State<AlbumPhotosGrid> createState() => _AlbumPhotosGridState();
}

class _AlbumPhotosGridState extends State<AlbumPhotosGrid> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Photos'),
      ),
      body: GridView.builder(
        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 300, mainAxisExtent: 300),
        itemBuilder: (context, index) {
          final photo = widget.photos[index];
          return Column(children: [
            Image.network(photo.thumbnailUrl!),
            FittedBox(
              fit: BoxFit.fitHeight,
              child: Text(
                photo.title ?? '',
                maxLines: 10,
                softWrap: true,
              ),
            )
          ]);
        },
        itemCount: widget.photos.length,
      ),
    );
  }
}

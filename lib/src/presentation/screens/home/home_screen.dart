import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:interview_task1/src/di/locator.dart';
import 'package:interview_task1/src/router/app_router.gr.dart';
import 'package:interview_task1/src/utils/shared_pref.dart';

import '../../../bloc/album/album_bloc.dart';

@RoutePage()
class HomeScreen extends StatefulWidget implements AutoRouteWrapper {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();

  @override
  Widget wrappedRoute(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt.get<AlbumBloc>()..add(FetchAlbumEvent()),
      child: this,
    );
  }
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Albums'),
        actions: [
          IconButton(
              onPressed: () {
                getIt
                    .get<SharedPref>()
                    .preferences
                    .setBool(SharedPrefKeys.isLoggedIn.key, false);
                context.router.replace(const AuthRoute());
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            context.read<AlbumBloc>().add(AddAlbumEvent());
          },
          child: const Icon(Icons.add)),
      body: BlocConsumer<AlbumBloc, AlbumState>(
        buildWhen: (previous, current) =>
            current is! AlbumDeleted &&
            current is! AlbumOperationFailure &&
            current is! AlbumPhotoLoaded,
        listener: (context, state) {
          //TODO convert to switch
          if (state is AlbumDeleted) {
            ScaffoldMessenger.of(context)
                .showSnackBar(const SnackBar(content: Text('Item deleted.')));
          } else if (state is AlbumOperationFailure) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text(state.message)));
          } else if (state is AlbumPhotoLoaded) {
            context.router.push(AlbumRoute(photos: state.photos));
          }
        },
        builder: (context, state) {
          if (state is AlbumLoaded) {
            final albums = state.albums?.albums ?? [];
            return ListView.builder(
              itemBuilder: (context, index) {
                return Dismissible(
                    background: Container(
                      padding: const EdgeInsets.all(8),
                      color: Colors.red,
                      alignment: Alignment.centerRight,
                      child: const Text('DELETE'),
                    ),
                    // Each Dismissible must contain a Key. Keys allow Flutter to
                    // uniquely identify widgets.
                    key: Key(albums[index].id.toString()),
                    // Provide a function that tells the app
                    // what to do after an item has been swiped away.
                    onDismissed: (direction) {
                      albums.removeAt(index);
                      context
                          .read<AlbumBloc>()
                          .add(DeleteAlbumEvent(id: albums[index].id));
                    },
                    child: ListTile(
                      title: Text(albums[index].title),
                      key: Key(albums[index].id.toString()),
                      trailing: Text(albums[index].id.toString()),
                      onTap: () {
                        context
                            .read<AlbumBloc>()
                            .add(LoadAlbumDetails(id: albums[index].id));
                      },
                    ));
              },
              itemCount: albums.length,
            );
          } else if (state is AlbumOperationFailure) {
            return const Center(
              child: Text('Failed to load albums'),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

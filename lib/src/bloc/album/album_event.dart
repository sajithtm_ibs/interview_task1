part of 'album_bloc.dart';

@immutable
sealed class AlbumEvent {}

class FetchAlbumEvent extends AlbumEvent {}

class DeleteAlbumEvent extends AlbumEvent {
  final int id;

  DeleteAlbumEvent({required this.id});
}

class AddAlbumEvent extends AlbumEvent {}

class LoadAlbumDetails extends AddAlbumEvent {
  final int id;

  LoadAlbumDetails({required this.id});
}

part of 'album_bloc.dart';

@immutable
sealed class AlbumState {}

final class AlbumInitial extends AlbumState {}

class AlbumOperationFailure extends AlbumState {
  final String message;

  AlbumOperationFailure({required this.message});
}

class AlbumLoaded extends AlbumState {
  final Albums? albums;

  AlbumLoaded({required this.albums});
}

final class AlbumDeleted extends AlbumState {}

final class AlbumPhotoLoaded extends AlbumState {
  final List<Photos> photos;

  AlbumPhotoLoaded({required this.photos});
}

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:interview_task1/src/data/model/photos.dart';
import 'package:interview_task1/src/data/repository/album_repo.dart';
import 'package:meta/meta.dart';

import '../../data/model/album.dart';

part 'album_event.dart';
part 'album_state.dart';

class AlbumBloc extends Bloc<AlbumEvent, AlbumState> {
  final AlbumRepo repo;

  AlbumBloc(this.repo) : super(AlbumInitial()) {
    fetchAlbum();
    deleteAlbum();
    addAlbum();
    on<LoadAlbumDetails>((event, emit) async {
      try {
        final photos = await repo.getPhotos(event.id);
        emit(AlbumPhotoLoaded(photos: photos));
      } catch (e) {
        emit(AlbumOperationFailure(message: 'Failed to get album photos'));
      }
    });
  }

  void addAlbum() => on<AddAlbumEvent>((event, emit) async {
        try {
          await repo.addToAlbum();
          emit(AlbumLoaded(albums: repo.albums));
        } catch (e) {
          emit(AlbumOperationFailure(message: 'Failed to add album'));
        }
      });

  void deleteAlbum() {
    return on<DeleteAlbumEvent>((event, emit) async {
      try {
        await repo.deleteAlbum(event.id);
        repo.albums?.albums?.removeWhere((element) => element.id == event.id);
        emit(AlbumLoaded(albums: repo.albums));
        // Small delay
        await Future.delayed(const Duration(microseconds: 10));
        emit(AlbumDeleted());
      } catch (e) {
        emit(AlbumOperationFailure(message: 'Failed to delete album'));
        await Future.delayed(const Duration(microseconds: 10));
        emit(AlbumLoaded(albums: repo.albums));
        debugPrint(e.toString());
      }
    });
  }

  void fetchAlbum() {
    return on<FetchAlbumEvent>((event, emit) async {
      try {
        await repo.loadAlbum();
        emit(AlbumLoaded(albums: repo.albums));
      } catch (e) {
        debugPrint(e.toString());
        emit(AlbumOperationFailure(message: 'Failed to load albums'));
      }
    });
  }
}

// ignore: depend_on_referenced_packages
import 'package:bloc/bloc.dart';
import 'package:interview_task1/src/data/repository/auth_repo.dart';
// ignore: depend_on_referenced_packages
import 'package:meta/meta.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepo repo;
  AuthBloc(this.repo) : super(AuthInitial()) {
    _onBoardStatus();

    _onValidateCredentials();
  }

  void _onBoardStatus() {
    return on<CheckOnBoardStatusEvent>((event, emit) {
      if (repo.isOnBoarded()) {
        emit(OnBoardSuccessState());
      }
    });
  }

  void _onValidateCredentials() {
    return on<SaveOrValidateCredentialsEvent>((event, emit) async {
      if (event.isOnboard) {
        await repo.saveCredentials(
            username: event.username, password: event.password);
        emit(OnBoardSuccessState());
        return;
      }

      final isValid = await repo.validateCredentials(
          username: event.username, password: event.password);

      repo.setLoggedInFlag();
      isValid
          ? emit(OnLoginSuccessState())
          : emit(OnFailureState(error: 'Invalid credentials'));
    });
  }
}

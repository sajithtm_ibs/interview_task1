part of 'auth_bloc.dart';

@immutable
sealed class AuthEvent {}

class SaveOrValidateCredentialsEvent extends AuthEvent {
  final String username;
  final String password;
  final bool isOnboard;

  SaveOrValidateCredentialsEvent(
      {required this.username,
      required this.password,
      required this.isOnboard});
}

class CheckOnBoardStatusEvent extends AuthEvent {}
